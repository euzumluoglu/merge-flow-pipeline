package com.emn.uzm.hook;

import java.util.HashMap;
import java.util.Map;

import com.atlassian.bitbucket.commit.NoSuchCommitException;
import com.atlassian.bitbucket.compare.CompareRef;
import com.atlassian.bitbucket.compare.CompareRequest;
import com.atlassian.bitbucket.compare.CompareRequest.Builder;
import com.atlassian.bitbucket.compare.CompareService;
import com.atlassian.bitbucket.hook.repository.RepositoryMergeRequestCheck;
import com.atlassian.bitbucket.hook.repository.RepositoryMergeRequestCheckContext;
import com.atlassian.bitbucket.pull.PullRequest;
import com.atlassian.bitbucket.pull.PullRequestRef;
import com.atlassian.bitbucket.repository.Repository;
import com.atlassian.bitbucket.setting.RepositorySettingsValidator;
import com.atlassian.bitbucket.setting.Settings;
import com.atlassian.bitbucket.setting.SettingsValidationErrors;

public class MergeFlowHook implements RepositoryMergeRequestCheck, RepositorySettingsValidator
{
	private static final String PREBRANCH = "refs/heads/";

	private final CompareService compareService;

	public MergeFlowHook(CompareService compareService) {
		this.compareService = compareService;
	}

	/**
	 * Vetos a pull-request if it is not suitable for workflow.
	 */
	// @Override
	public void check(RepositoryMergeRequestCheckContext context) {
		PullRequest pullRequest = context.getMergeRequest().getPullRequest();
		String flowSchema = context.getSettings().getString("flowSchema");
		PullRequestRef fromRef = pullRequest.getFromRef();
		CompareRef fromCompare = new CompareRef(fromRef);
		PullRequestRef toRef = pullRequest.getToRef();
		Repository toRepository = toRef.getRepository();
		Map<String, String> flowMap = getFlowSchemaMap(flowSchema);
		String beforeBranch = flowMap.get(toRef.getDisplayId());
		if (beforeBranch == null || beforeBranch.isEmpty()) {
			return;
		}
		Builder compareBuilder = new CompareRequest.Builder();
		compareBuilder.fromRef(fromCompare);
		compareBuilder.toRef(PREBRANCH + beforeBranch, toRepository);
		try {

			CompareRequest compareRequest = compareBuilder.build();
			ConcreateCommitCallback commitCallBack = new ConcreateCommitCallback();
			compareService.streamCommits(compareRequest, commitCallBack);
			if (commitCallBack.isDiffExist()) {
				context.getMergeRequest().veto("Pull Request is not suitable for repository workflow", "Branch \""
						+ fromRef.getDisplayId() + "\" must be merged to \"" + beforeBranch + "\" before");
			}
		} catch (NoSuchCommitException e) {
			context.getMergeRequest().veto("Wrong workflow configuration",
					"Merge workflow order is \""+flowSchema+"\" and Branch \"" + beforeBranch + "\" is not exist. "//
							+ "Please contact your Administrator in your team");
		}
	}

	private String getDisplayBranchName(String branchName) {
		if (branchName.indexOf(PREBRANCH) >= 0) {
			return branchName.substring(PREBRANCH.length());
		}
		return branchName;
	}

	private Map<String, String> getFlowSchemaMap(String flowSchema) {
		Map<String, String> flowMap = new HashMap<String, String>();
		if (flowSchema == null || flowSchema.isEmpty()) {
			return flowMap;
		}
		String[] branchList = flowSchema.split(" ");
		for (int i = 1; i < branchList.length; i++) {
			flowMap.put(getDisplayBranchName(branchList[i]), getDisplayBranchName(branchList[i - 1]));
		}
		return flowMap;
	}

	// @Override
	public void validate(Settings settings, SettingsValidationErrors errors, Repository repository) {

	}
}
