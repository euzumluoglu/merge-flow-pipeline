package com.emn.uzm.hook;

import java.io.IOException;
import java.util.Collection;

import com.atlassian.bitbucket.commit.AbstractCommitCallback;
import com.atlassian.bitbucket.commit.Commit;
import com.atlassian.bitbucket.commit.CommitContext;
import com.atlassian.bitbucket.commit.CommitSummary;
import com.atlassian.bitbucket.commit.MinimalCommit;

public class ConcreateCommitCallback extends AbstractCommitCallback {
	
	private boolean isDiffExist = false;
	public ConcreateCommitCallback() {
		super();
	}

	public boolean onCommit(Commit commit) throws IOException {
		Collection<MinimalCommit> parents = commit.getParents();
		if(parents.size()==1){
			isDiffExist = true;
			return false;
		}
		return super.onCommit(commit);
	}

	public void onEnd(CommitSummary summary) throws IOException {
		super.onEnd(summary);

	}

	public void onStart(CommitContext context) throws IOException {
		super.onStart(context);
	}
	
	public boolean isDiffExist(){
		return isDiffExist;
	}
}
